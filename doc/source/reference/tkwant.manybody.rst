:mod:`tkwant.manybody` -- Solving the many-body time-dependent Schrödinger equation
===================================================================================

.. module:: tkwant.manybody

States
------
Solving the many-body time-dependent Schrödinger equation.

.. autosummary::
    :toctree: generated

    State
    WaveFunction

Helper functions
----------------

.. autosummary::
    :toctree: generated

    lead_occupation
    calc_intervals
    split_intervals
    combine_intervals
    calc_tasks
    calc_initial_state
    calc_energy_cutoffs
    make_boundstates_time_dependent
    add_boundstates


Data types
----------

.. autosummary::
    :toctree: generated

    Occupation
    Interval


