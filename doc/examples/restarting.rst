:orphan:

.. _restarting:

Restarting calculations from previously saved results
=====================================================

The physical system in this example is similar to :ref:`open_system`.
For time arguments on the right of the dashed vertical line, the
result is calculated with the restarted wave function.

**tkwant features highlighted**

-  restarting calculations from previously saved results

.. jupyter-execute:: restarting.py

.. seealso::
    The complete source code of this example can be found in
    :download:`restarting.py <restarting.py>`.


